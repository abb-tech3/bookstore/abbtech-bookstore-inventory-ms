package com.abbtech.inventory.controller;

import java.util.UUID;
import com.abbtech.inventory.dto.request.AddBookRequestDto;
import com.abbtech.inventory.service.impl.BookServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping()
public class InventoryController {

    private final BookServiceImpl bookService;

    @PostMapping("/create")
    public void createBook(@RequestBody AddBookRequestDto addBookRequestDto) {
        bookService.addBook(addBookRequestDto);
    }

    @GetMapping("/inventorycheck")
    public boolean checkBook(@RequestParam("bookId") UUID bookId) {
        return bookService.isBookExist(bookId);
    }

}
