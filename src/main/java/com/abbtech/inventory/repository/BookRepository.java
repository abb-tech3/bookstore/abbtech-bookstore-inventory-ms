package com.abbtech.inventory.repository;

import java.util.Optional;
import java.util.UUID;
import com.abbtech.inventory.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends JpaRepository<Book, UUID> {


    Optional<Book> findById(UUID uuid);


}
