package com.abbtech.inventory.dto.request;

import java.math.BigDecimal;
import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddBookRequestDto {

    @NotNull
    private String title;
    @NotNull
    private String author;

    @Digits(integer = 19, fraction = 2)
    private BigDecimal price;
}
