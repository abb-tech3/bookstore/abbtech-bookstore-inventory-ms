package com.abbtech.inventory.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import com.abbtech.inventory.dto.request.AddBookRequestDto;
import com.abbtech.inventory.entity.Book;
import com.abbtech.inventory.repository.BookRepository;
import com.abbtech.inventory.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    @Override
    public void addBook(AddBookRequestDto addBookRequestDto) {
        Book book = new Book();
        book.setId(UUID.randomUUID());
        book.setTitle(addBookRequestDto.getTitle());
        book.setAuthor(addBookRequestDto.getAuthor());
        book.setPrice(addBookRequestDto.getPrice());
        bookRepository.save(book);
    }

    @Override
    public List<Book> getBooks() {
        return bookRepository.findAll();
    }

    @Override
    public Optional<Book> getBook(UUID uuid) {
       return bookRepository.findById(uuid);
    }

    @Override
    public boolean isBookExist(UUID uuid) {
        return bookRepository.findById(uuid).isPresent();
    }


}
