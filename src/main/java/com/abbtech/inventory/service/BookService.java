package com.abbtech.inventory.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import com.abbtech.inventory.dto.request.AddBookRequestDto;
import com.abbtech.inventory.entity.Book;

public interface BookService {
    void addBook(AddBookRequestDto addBookRequestDto);
    List<Book> getBooks();
    Optional<Book> getBook(UUID uuid);

    boolean isBookExist(UUID uuid);
}
